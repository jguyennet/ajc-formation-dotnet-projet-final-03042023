﻿using cadeau.api;
using cadeau.api.adapters;
using cadeau.api.Exceptions;

var consoleAfficheur = new ConsoleAfficheur();
var consoleLecteurInfos = new ConsoleLecteurInfos();
PreparerAbonnes();

void EnregistrerResultatsDansFichier(GestionCadeaux gestionCadeaux)
{
    // Path.Combine(Environment.CurrentDirectory, "resultats.txt") retourne le chemin complet du fichier resultats.txt dans le dossier courant (ex: C:\Users\...\resultats.txt) 
    var cheminFichier = Path.Combine(Environment.CurrentDirectory, "resultats.txt");

    List<string> contenuFichier = new List<string>();

    try
    {
        contenuFichier.Add("Attribution des cadeaux pour les abonnés:");
        contenuFichier.Add("");

        // Pour chaque abonné de la liste des abonnés on ajoute les informations dans le contenu du fichier
        foreach (var abonne in gestionCadeaux.ListeAbonnes)
        {
            contenuFichier.Add($"Abonné : {abonne.Prenom}");
            contenuFichier.Add($"Email : {abonne.Email}");
            // Si l'abonné a reçu un cadeau on ajoute le nom du cadeau dans le contenu du fichier, sinon on ajoute "Aucun"
            if (abonne.Cadeau != null)
            {
                contenuFichier.Add($"Cadeau : {abonne.Cadeau.Nom}");
            }
            else
            {
                contenuFichier.Add("Cadeau : Aucun");
            }
            contenuFichier.Add("");
        }

        string contenuTexte = "";

        // Ici on ajoute les lignes du contenu du fichier dans une variable de type string pour pouvoir l'écrire dans le fichier texte 
        foreach (string ligne in contenuFichier)
        {
            // Environment.NewLine permet de retourner à la ligne dans le fichier texte
            contenuTexte += ligne + Environment.NewLine;
        }

        // On écrit le contenu du fichier dans le fichier texte 
        File.WriteAllText(cheminFichier, contenuTexte);

        consoleAfficheur.Afficher("Les résultats ont été enregistrés dans le fichier resultats.txt.");
    }

    // Ici on gère les exceptions pouvant être levées lors de l'écriture  dans le fichier texte
    // PathTooLongException est levée si le chemin du fichier est trop long 
    catch (PathTooLongException ex)
    {
        consoleAfficheur.Afficher($"Erreur : Le chemin du fichier est trop long. {ex.Message}");
    }
    // IoException est levée si le fichier est en lecture seule ou si le fichier est déjà ouvert dans un autre programme             
    catch (IOException ex)
    {
        consoleAfficheur.Afficher($"Erreur : Problème d'entrée/sortie. {ex.Message}");
    }

}


// Fonction qui permet de préparer les abonnés
void PreparerAbonnes()
{
    // On crée une liste d'abonnés
    List<Abonne> ListeAbonnes = new List<Abonne>
    {
        new Abonne("Jean", "jean@orange.fr", 3.5),
        new Abonne("Marc", "marc@orange.fr", 1.5),
        new Abonne("Elise", "elise@orange.fr", 1),
        new Abonne("Paul", "paul@orange.fr", 4),
        new Abonne("Marie", "marie@orange.fr", 8),
        new Abonne("Lucie", "lucie@orange.fr", 5),
    };

    // On demande à l'utilisateur s'il veut ajouter un abonné à la liste d'abonnés existante ou non 
    DemandeAjoutAbonne(ListeAbonnes);

    // Création de la liste des cadeaux
    List<Cadeau> ListeCadeaux = CreerListeCadeaux();

    // On demande à l'utilisateur s'il veut ajouter un cadeau à la liste de cadeaux existante ou non
    DemandeAjoutCadeau(ListeCadeaux);

    // Création de l'instance GestionCadeaux
    var gestionCadeaux = new GestionCadeaux(consoleAfficheur, ListeAbonnes, ListeCadeaux);

    // Attribution des cadeaux et affichage des résultats
    try
    {
        gestionCadeaux.AttribuerCadeaux();
        gestionCadeaux.AfficherResultats();
        EnregistrerResultatsDansFichier (gestionCadeaux); // On enregistre les résultats dans un fichier 
    }
    catch (ListeAbonnesVideException ex)
    {
        consoleAfficheur.Afficher($"Erreur : {ex.Message}");
    }
    catch (ListeCadeauxVideException ex)
    {
        consoleAfficheur.Afficher($"Erreur : {ex.Message}");
    }
}


// Fonction qui permet de demander à l'utilisateur s'il veut ajouter un abonné à la liste d'abonnés existante ou non
void DemandeAjoutAbonne(List<Abonne> ListeAbonnes)
{
    string reponse;
    do
    {
        consoleAfficheur.Afficher("Voulez-vous ajouter un abonné à la liste ? (O/N)");
        reponse = consoleLecteurInfos.LireLigne();
        if (reponse == "O")
        {
            AjouterAbonne(ListeAbonnes);
        }
        else if (reponse == "N")
        {
            consoleAfficheur.Afficher("Vous avez choisi de ne pas ajouter d'abonné à la liste");           
        }
        else
        {
            consoleAfficheur.Afficher("Vous n'avez pas saisi une réponse valide");
            DemandeAjoutAbonne(ListeAbonnes);
        }
    }
    while (reponse != "N");
}

// Fonction qui permet d'ajouter un abonné à la liste d'abonnés existante
void AjouterAbonne(List<Abonne> ListeAbonnes)
{
    consoleAfficheur.Afficher("Vous avez choisi d'ajouter un abonné à la liste");
    consoleAfficheur.Afficher("Veuillez saisir le prénom de l'abonné :");
    string nom = consoleLecteurInfos.LireLigne();
    consoleAfficheur.Afficher("Veuillez saisir l'adresse mail de l'abonné :");
    string mail = consoleLecteurInfos.LireLigne();
    consoleAfficheur.Afficher("Veuillez saisir le nombre d'heures de présence de l'abonné :");
    double nbHeuresPresence = Convert.ToDouble(consoleLecteurInfos.LireLigne());
    Abonne abonne = new Abonne(nom, mail, nbHeuresPresence);
    ListeAbonnes.Add(abonne);
    consoleAfficheur.Afficher("L'abonné a bien été ajouté à la liste");
}

// Fonction pour créer la liste de cadeaux
static List<Cadeau> CreerListeCadeaux()
{
    var reduction = 10;
    var tempsAvecStreamer = 1;
    var jeuAAprendre = "The Witcher";
    var jeuAOffrir = "Skyrim";

    List<Cadeau> ListeCadeaux = new List<Cadeau>
        {
            new CadeauReduction($"Réduction de {reduction} % sur Steam", reduction),
            new CadeauOffert($"{jeuAOffrir} offert", jeuAOffrir),
            new CadeauTempsAvecStreamer($"Temps gagné avec le Streamer : {tempsAvecStreamer}h", tempsAvecStreamer),
            new CadeauApprendreJeu($"Apprendre à jouer à {jeuAAprendre}", jeuAAprendre)
        };

    return ListeCadeaux;
}

// Fonction qui permet de demander à l'utilisateur s'il veut ajouter un cadeau à la liste de cadeaux existante ou non
void DemandeAjoutCadeau(List<Cadeau> ListeCadeaux)
{
    string reponse;
    do
    {
        consoleAfficheur.Afficher("Voulez-vous ajouter un cadeau à la liste ? (O/N)");
        reponse = consoleLecteurInfos.LireLigne();
        if (reponse == "O")
        {
            AjouterCadeau(ListeCadeaux);
        }
        else if (reponse == "N")
        {
            consoleAfficheur.Afficher("Vous avez choisi de ne pas ajouter de cadeau à la liste");
            //PreparerCadeaux();
        }
        else
        {
            consoleAfficheur.Afficher("Vous n'avez pas saisi une réponse valide");
            DemandeAjoutCadeau(ListeCadeaux);
        }
    }
    while (reponse != "N");
}

// Fonction qui permet d'ajouter un cadeau
void AjouterCadeau(List<Cadeau> ListeCadeaux)
{
    Console.WriteLine("Vous avez choisi d'ajouter un cadeau à la liste");
    Console.WriteLine("Veuillez choisir le type de cadeau à ajouter:");
    Console.WriteLine("1. Réduction");
    Console.WriteLine("2. Jeu offert");
    Console.WriteLine("3. Temps avec le streamer");
    Console.WriteLine("4. Apprendre un jeu");

    int typeCadeau = Convert.ToInt32(consoleLecteurInfos.LireLigne());

    switch (typeCadeau)
    {
        case 1:
            Console.WriteLine("Veuillez saisir le pourcentage de réduction:");
            int reduction = Convert.ToInt32(consoleLecteurInfos.LireLigne());
            ListeCadeaux.Add(new CadeauReduction($"Réduction de {reduction} % sur Steam", reduction));
            break;
        case 2:
            Console.WriteLine("Veuillez saisir le nom du jeu à offrir:");
            string jeuAOffrir = consoleLecteurInfos.LireLigne();
            ListeCadeaux.Add(new CadeauOffert($"{jeuAOffrir} offert", jeuAOffrir));
            break;
        case 3:
            Console.WriteLine("Veuillez saisir le nombre d'heures avec le streamer:");
            int tempsAvecStreamer = Convert.ToInt32(consoleLecteurInfos.LireLigne());
            ListeCadeaux.Add(new CadeauTempsAvecStreamer($"Temps gagné avec le Streamer : {tempsAvecStreamer}h", tempsAvecStreamer));
            break;
        case 4:
            Console.WriteLine("Veuillez saisir le nom du jeu à apprendre:");
            string jeuAAprendre = consoleLecteurInfos.LireLigne();
            ListeCadeaux.Add(new CadeauApprendreJeu($"Apprendre à jouer à {jeuAAprendre}", jeuAAprendre));
            break;
        default:
            Console.WriteLine("Type de cadeau invalide. Veuillez réessayer.");
            AjouterCadeau(ListeCadeaux);
            break;
    }   
}