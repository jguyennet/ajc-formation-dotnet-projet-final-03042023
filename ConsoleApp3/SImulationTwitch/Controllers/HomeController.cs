﻿using Microsoft.AspNetCore.Mvc;
using SImulationTwitch.Models;
using System.Diagnostics;

namespace SImulationTwitch.Controllers
{
    /// <summary>
    ///  Le controller Home est le controller par défaut de l'application (il est appelé quand on lance l'application)
    /// </summary>
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly DefaultDbContext context;
        
        public HomeController(ILogger<HomeController> logger, DefaultDbContext context)
        {
            _logger = logger;
            this.context = context;
        }
        /// <summary>
        ///  On récupère la liste des streams et on les envoie à la vue Index.cshtml
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {           
            // ToList permet de transformer un IEnumerable en List (ou autre collection) 
            var streams = this.context.Streams.ToList();
            return View(streams);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}