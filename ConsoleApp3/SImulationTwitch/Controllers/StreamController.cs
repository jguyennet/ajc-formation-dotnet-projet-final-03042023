﻿using Microsoft.AspNetCore.Mvc;
using SImulationTwitch.Models;
// Ici on importe la classe Stream qui se trouve dans le namespace SImulationTwitch.Models
using Stream = SImulationTwitch.Models.Stream;

namespace SImulationTwitch.Controllers
{
    /// <summary>
    /// Le controller Stream est le controller qui gère les streams
    /// </summary>
    public class StreamController : Controller
    {
        private readonly DefaultDbContext context;
        /// <summary>
        /// On injecte le contexte de données dans le controller pour pouvoir utiliser les données de la base de données
        /// </summary>
        /// <param name="context"></param>
        public StreamController(DefaultDbContext context)
        {
            this.context = context;
        }

        public IActionResult Add()
        {
            return View();
        }     

        [HttpPost]
        public IActionResult Add(Stream stream)
        {
            // On ajoute le stream à la liste des streams
            this.context.Streams.Add(stream); 
            // On sauvegarde les changements dans la base de données
            this.context.SaveChanges();
            
            return RedirectToAction("Index", "Home");
        }
    }
}
