﻿using Microsoft.EntityFrameworkCore;

namespace SImulationTwitch.Models
{
    /// <summary>
    /// DbContext est une classe fournie par Entity Framework Core.
    /// </summary>
    public class DefaultDbContext : DbContext 
    {
        
        public DefaultDbContext(DbContextOptions options) : base(options)
        {
        }

        protected DefaultDbContext()
        {
        }

        /// <summary>
        ///  C'est une liste ! Et elle sera connectée à une table en bdd
        /// </summary>
        public DbSet<Stream> Streams { get; set; }
    }
}
