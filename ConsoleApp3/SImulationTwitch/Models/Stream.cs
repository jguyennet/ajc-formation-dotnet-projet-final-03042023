﻿namespace SImulationTwitch.Models
{
    public class Stream
    {       
        public int Id { get; set; }
        public string Titre { get; set; }
        public DateTime Date { get; set; }
        public string Status
        {
            get
            {
                var now = DateTime.Now;
                // Si la date du stream est supérieur à la date actuelle alors le stream est à venir
                if (Date > now)
                {
                    return "A venir";
                }
                // Si la date du stream est inférieur à la date actuelle et que la date du stream + 1 heure est supérieur à la date actuelle alors le stream est en cours. On définit donc qu'un stream dure 1 heure maximum pour simplifier le code.
                else if (Date <= now && Date.AddHours(1) >= now)
                { 
                    return "En cours";
                }
                // Sinon le stream est fini
                else
                {
                    return "Fini";
                }
            }
        }
    }
}
