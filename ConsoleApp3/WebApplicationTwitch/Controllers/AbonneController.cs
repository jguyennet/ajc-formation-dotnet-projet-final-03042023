﻿using Microsoft.AspNetCore.Mvc;
using WebApplicationTwitch.Models;

namespace WebApplicationTwitch.Controllers
{
    /// <summary>
    ///  AbonneController est une classe qui hérite de la classe Controller fournie par ASP.NET Core MVC 
    /// </summary>
    public class AbonneController : Controller
    {
        /// <summary>
        /// context est une propriété privée de type DefaultDbContext qui va nous permettre de récupérer les données en bdd 
        /// </summary>
        private readonly DefaultDbContext context;

        /// <summary>
        /// Comme j'ai ajouté la ligne builder.Services.AddDbContext<DefaultDbContext>() dans le programme, alors ici on va récupérer une instance de DefaultDbContext déjà configurée et prête à l'emploi. 
        /// </summary>
        /// <param name="context"></param>
        public AbonneController(DefaultDbContext context)
        {
            this.context = context;
        }
        /// <summary>
        ///  On récupère la liste des abonnés et on les envoie à la vue List.cshtml
        /// </summary>
        /// <param name="filtre"></param>
        /// <returns></returns>
        public IActionResult List(string filtre)
        {           
            // Ici on récupère le premier gamer de la bdd
            Gamer gamer = context.Gamers.First(); 

            // On calcule le seuil de présence à partir du nombre total de streams et du pourcentage de présence (70%). Ce seuil va ainsi nous permettre de déterminer si un abonné est présent dans au moins 70% des streams ou non.
            double seuilPresence = gamer.NombreTotalDeStreams * 0.7;
            
            // On récupère la liste des abonnés
            var query = from item in context.Abonnes
                        select item;
            
            // filtre est la valeur de l'attribut value de l'option sélectionnée dans la liste déroulante du formulaire de la vue List.cshtml 
            if (filtre == "connectes")
            {
                // On filtre la liste des abonnés pour ne garder que ceux qui sont connectés 
                query = from item in query
                        where item.EstConnecte
                        select item;
            }
            else if (filtre == "presents")
            {
                // On filtre la liste des abonnés pour ne garder que ceux qui sont présents dans au moins 70% des streams
                query = from item in query
                        where item.NombreDeConnexions >= seuilPresence
                        select item;
            }

            string title;
            // Ici on va définir le titre <h2> de la page en fonction de la valeur de l'attribut value de l'option sélectionnée dans la liste déroulante du formulaire de la vue List.cshtml 
            switch (filtre)
            {
                case "connectes":
                    title = "Liste des abonnés connectés";
                    break;
                case "presents":
                    title = "Liste des abonnés les plus présents";
                    break;
                default:
                    title = "Liste de tous les abonnés";
                    break;
            }
            ViewData["Title"] = title;


            // On retourne la vue List.cshtml avec la liste des abonnés filtrée en paramètre
            return View("List", query.ToList());

        }
    }
}
