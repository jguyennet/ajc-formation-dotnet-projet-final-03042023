﻿namespace WebApplicationTwitch.Models
{
    /// <summary>
    /// Classe Abonne qui représente un abonné de Twitch 
    /// </summary>
    public class Abonne
    {
        public int Id { get; set; }
        public string Surnom { get; set; }
        public string ImageProfil { get; set; }
        public bool EstConnecte { get; set; }
        public int NombreDeConnexions { get; set; }
        /// <summary>
        /// Constructeur par défaut de la classe Abonne 
        /// </summary>
        public Abonne()
        {
        }
        /// <summary>
        /// Constructeur de la classe Abonne avec paramètres 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="surnom"></param>
        /// <param name="imageProfil"></param>
        /// <param name="estConnecte"></param>
        public Abonne(int id, string surnom, string imageProfil, bool estConnecte)
        {
            Id = id;
            Surnom = surnom;
            ImageProfil = imageProfil;
            EstConnecte = estConnecte;
        }
    }
}
