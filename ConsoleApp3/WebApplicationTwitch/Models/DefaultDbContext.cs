﻿using Microsoft.EntityFrameworkCore;

namespace WebApplicationTwitch.Models
{
    /// <summary>
    /// DbContext est une classe fournie par Entity Framework Core.
    /// </summary>
    public class DefaultDbContext : DbContext 
    {
        
        public DefaultDbContext(DbContextOptions options) : base(options)
        {
        }

        protected DefaultDbContext()
        {
        }

        /// <summary>
        /// Ici on définit une liste d'Abonnes qui sera connectée à une table en bdd (Abonnes)
        /// </summary>
        public DbSet<Abonne> Abonnes { get; set; }
        /// <summary>
        /// Ici on définit une liste de Gamers qui sera connectée à une table en bdd (Gamers)
        /// </summary>
        public DbSet<Gamer> Gamers { get; set; }
    }
}
