﻿namespace WebApplicationTwitch.Models
{
    /// <summary>
    /// Classe Gamer qui représente un gamer de Twitch
    /// </summary>
    public class Gamer
    {
        public int Id { get; set; }
        public int NombreTotalDeStreams { get; set; }
        /// <summary>
        ///  Constructeur de la classe Gamer. Il permet de créer un objet Gamer sans paramètres 
        /// </summary>
        public Gamer()
        {
        }
        /// <summary>
        ///  Constructeur de la classe Gamer avec paramètres 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nombreTotalDeStreams"></param>
        public Gamer(int id, int nombreTotalDeStreams)
        {
            Id = id;
            NombreTotalDeStreams = nombreTotalDeStreams;
        }
    }
}
