﻿namespace cadeau.api
{
    /// <summary>
    /// Classe représentant un abonné
    /// </summary>
    public class Abonne
    {
        public string Prenom { get; set; }
        public string Email { get; set; }
        public Cadeau Cadeau { get; set; }
        public double DureePresence { get; set; }

        /// <summary>
        /// Constructeur de la classe Abonne
        /// </summary>
        /// <param name="prenom"></param>
        /// <param name="email"></param>
        /// <param name="dureePresence"></param>
        public Abonne(string prenom, string email, double dureePresence)
        {
            Prenom = prenom;
            Email = email;
            DureePresence = dureePresence;
        } 

        /// <summary>
        /// Méthode permettant de recevoir un cadeau
        /// </summary>
        /// <param name="cadeau"></param>
        public void RecevoirCadeau(Cadeau cadeau)
        {
            Cadeau = cadeau;
        }
    }
}
