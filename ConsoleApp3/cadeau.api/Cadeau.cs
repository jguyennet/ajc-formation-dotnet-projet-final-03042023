﻿namespace cadeau.api
{
    /// <summary>
    /// Ici on définit la classe Cadeau
    /// </summary>
    public class Cadeau
    {
        public string Nom { get; set; }

        /// <summary>
        /// Constructeur de la classe Cadeau
        /// </summary>
        /// <param name="nom"></param>
        public Cadeau(string nom)
        {
            Nom = nom;
        }
    }
}
