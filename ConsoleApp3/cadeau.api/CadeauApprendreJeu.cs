﻿namespace cadeau.api
{
    /// <summary>
    /// Classe représentant un cadeau dont le but est d'apprendre un jeu. Cette classe hérite de la classe Cadeau.
    /// </summary>
    public class CadeauApprendreJeu : Cadeau
    {
        public string Jeu { get; set; }

        /// <summary>
        /// Constructeur de la classe CadeauApprendreJeu
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="jeu"></param>
        public CadeauApprendreJeu(string nom, string jeu)
            : base(nom)
        {
            Jeu = jeu;
        }
    }
}
