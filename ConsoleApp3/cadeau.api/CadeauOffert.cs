﻿namespace cadeau.api
{   
    /// <summary>
    /// Classe représentant un cadeau offert. Cette classe hérite de la classe Cadeau.
    /// </summary>
    public class CadeauOffert : Cadeau
    {
        public string JeuOffert { get; set; }
        /// <summary>
        /// Constructeur de la classe CadeauOffert
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="jeuOffert"></param>
        public CadeauOffert(string nom, string jeuOffert)
            : base(nom)
        {
            JeuOffert = jeuOffert;
        }
    }
}
