﻿namespace cadeau.api
{
    /// <summary>
    /// Classe représentant un cadeau de type réduction. Cette classe hérite de la classe Cadeau.
    /// </summary>
    public class CadeauReduction : Cadeau
    {
        public float PourcentageReduction { get; set; }        
        public CadeauReduction(string nom, float pourcentageReduction) : base(nom)
        {
            PourcentageReduction = pourcentageReduction;
        }
    }
}
