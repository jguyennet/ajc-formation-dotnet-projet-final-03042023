﻿namespace cadeau.api
{
    /// <summary>
    /// Classe représentant un cadeau permettant de passer du temps avec le streamer. Cette classe hérite de la classe Cadeau.
    /// </summary>
    public class CadeauTempsAvecStreamer : Cadeau
    {
        public float Duree { get; set; }
        /// <summary>
        /// Constructeur de la classe CadeauTempsAvecStreamer
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="duree"></param>
        public CadeauTempsAvecStreamer(string nom, float duree)
            : base(nom)
        {
            Duree = duree;
        }
    }
}
