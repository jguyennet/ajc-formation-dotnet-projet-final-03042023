﻿using cadeau.api.adapters;

namespace cadeau.api
{
    /// <summary>
    /// Afficheur qui affiche sur la console
    /// </summary>
    public class ConsoleAfficheur : IAfficheur
    {
        /// <summary>
        /// Affiche le texte passé en paramètre sur la console 
        /// </summary>
        /// <param name="texte"></param>
        public void Afficher(string texte)
        {
            Console.WriteLine(texte);
        }
    }
}
