﻿using cadeau.api.adapters;

namespace cadeau.api
{
    /// <summary>
    /// Lecteur d'infos qui lit sur la console
    /// </summary>
    public class ConsoleLecteurInfos : ILecteurInfos
    {
        public string LireLigne()
        {
            return Console.ReadLine();
        }
    }
}
