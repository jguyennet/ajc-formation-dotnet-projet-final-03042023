﻿namespace cadeau.api.Exceptions
{
    /// <summary>
    /// Exception levée quand la liste des abonnés est vide 
    /// </summary>
    public class ListeAbonnesVideException : Exception
    {
        public ListeAbonnesVideException(string message) : base(message)
        {
        }
    }
}
