﻿namespace cadeau.api.Exceptions
{
    /// <summary>
    /// Exception levée quand la liste des cadeaux est vide
    /// </summary>
    public class ListeCadeauxVideException : Exception
    {
        public ListeCadeauxVideException(string message) : base(message)
        {
        }
    }
}
