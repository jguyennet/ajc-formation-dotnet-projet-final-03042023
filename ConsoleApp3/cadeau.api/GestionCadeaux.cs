﻿using cadeau.api.adapters;
using cadeau.api.Exceptions;

namespace cadeau.api
{
    /// <summary>
    ///   Classe qui gère l'attribution des cadeaux aux abonnés et l'affichage des résultats 
    /// </summary>
    public class GestionCadeaux
    {
        public readonly IAfficheur afficheur;
        public List<Abonne> ListeAbonnes { get; set; }
        public List<Cadeau> ListeCadeaux { get; set; }

        /// <summary>
        /// Constructeur de la classe GestionCadeaux
        /// </summary>
        /// <param name="afficheur"></param>
        /// <param name="ListeAbonnes"></param>
        /// <param name="ListeCadeaux"></param>
        public GestionCadeaux(IAfficheur afficheur, List<Abonne> ListeAbonnes, List<Cadeau> ListeCadeaux)
        {
            this.afficheur = afficheur;
            this.ListeAbonnes = ListeAbonnes;
            this.ListeCadeaux = ListeCadeaux;
        }

        /// <summary>
        ///  Méthode qui attribue des cadeaux aux abonnés qui ont été présents au moins 2 heures, avec une probabilité de 50%.
        /// </summary>
        /// <exception cref="ListeAbonnesVideException"></exception>
        /// <exception cref="ListeCadeauxVideException"></exception>
        public void AttribuerCadeaux()
        {
            // Vérifier que la liste des abonnés n'est pas vide (sinon, lever une exception) : 
            if (ListeAbonnes.Count == 0)
            {
                throw new ListeAbonnesVideException("La liste des abonnés est vide");
            }
            // Vérifier que la liste des cadeaux n'est pas vide (sinon, lever une exception) :
            if (ListeCadeaux.Count == 0)
            {
                throw new ListeCadeauxVideException("La liste des cadeaux est vide");
            }

            Random random = new Random();

            foreach (Abonne abonne in ListeAbonnes)
            {
                // Si l'abonné a été présent au moins 2 heures alors il a 50% de chance de recevoir un cadeau
                if (abonne.DureePresence >= 2 && random.NextDouble() < 0.5)
                {
                    // random.Next(ListeCadeaux.Count) retourne un entier compris entre 0 et le nombre de cadeaux - 1 (exclu)
                    int indexCadeau = random.Next(ListeCadeaux.Count);
                    // L'abonné reçoit le cadeau à l'index indexCadeau de la liste des cadeaux 
                    abonne.RecevoirCadeau(ListeCadeaux[indexCadeau]);
                }
            }
        }

        /// <summary>
        ///  Méthode qui affiche les résultats d'attribution des cadeaux (prénom, email et cadeau)
        /// </summary>
        public void AfficherResultats()
        {
            afficheur.Afficher("Résultats du tirage au sort des cadeaux :");
            afficheur.Afficher("Prénom  -  Email  -  Cadeau");
            afficheur.Afficher("--------------------------------------");

            foreach (Abonne abonne in ListeAbonnes)
            {
                // cadeau est égal au nom du cadeau si l'abonné en a reçu un, sinon il est égal à une chaîne vide
                string cadeau = abonne.Cadeau != null ? abonne.Cadeau.Nom : "";
                afficheur.Afficher($"{abonne.Prenom}  -  {abonne.Email}  -  {cadeau}");
            }
        }

        
    }
}
