﻿namespace cadeau.api.adapters
{
    /// <summary>
    /// Définition de l'interface qui permet d'afficher des infos
    /// </summary>
    public interface IAfficheur
    {
        /// <summary>
        /// Affiche le texte passé en paramètre
        /// </summary>
        /// <param name="texte"></param>
        void Afficher(string texte);
    }
}

