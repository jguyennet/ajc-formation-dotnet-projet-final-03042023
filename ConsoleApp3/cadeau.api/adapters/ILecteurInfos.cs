﻿namespace cadeau.api.adapters
{
    /// <summary>
    /// Définition de l'interface qui permet de lire des infos
    /// </summary>
    public interface ILecteurInfos
    {
        string LireLigne();
    }
}
